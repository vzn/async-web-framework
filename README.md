# 简单的异步非阻塞Web框架
>异步非阻塞Web框架内部原理:基于非阻塞的Socket以及IO多路复用从而实现异步非阻塞的Web框架

    
- [异步IO模块](https://github.com/cvno/plugins/tree/master/io)：
	客户端角色，socket客户端

- [Web框架](/code.py)：
	服务端角色，socket服务端

## 使用

- [基本使用](/1.py)
没有使用异步功能
- [异步非阻塞](/2.py)
基于等待模式可以完成自定制操作

Future对象，set_result(改变它的值)

