from code import Snow
from code import HttpResponse
from code import Future

request_list = []


def callback(request, future):
    return HttpResponse(future.value)


def req(request):
    print('客官来了')
    obj = Future(callback=callback)
    request_list.append(obj)
    yield obj # -> gen

# 手动 set_result
def stop(request):
    obj = request_list[0]
    del request_list[0]
    obj.set_result('done')
    return HttpResponse('stop')


routes = [
    (r'/req/', req),
    (r'/stop/', stop),
]

app = Snow(routes)
app.run(port=8012)