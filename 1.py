from code import Snow
from code import HttpResponse


def index(request):
    return HttpResponse('OK')

# 路由
routes = [
    (r'/index/', index),
]

app = Snow(routes)
app.run(port=8012)